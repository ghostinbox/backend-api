from flask import Flask, request, jsonify
import requests
import mysql.connector
import bcrypt
import subprocess
import os
import base64
import secrets
import string
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)

# Replace these values with your MariaDB credentials
db_config = {
    'host': os.getenv("GHOSTINBOX_DB_HOST"),
    'user': os.getenv("GHOSTINBOX_DB_USER"),
    'password': os.getenv("GHOSTINBOX_DB_SECRET"),
    'database': os.getenv("GHOSTINBOX_DB_NAME")
}

# Create a connection to MariaDB
conn = mysql.connector.connect(**db_config)
cursor = conn.cursor()

class Account:
    def __init__(self, username, description="", secret=""):
        self.username = username

        if secret != "":
            self.secret = secret
        else:
            query = 'SELECT secret FROM accounts WHERE name = %s'
            values = (self.username,)
            cursor.execute(query, values)
            self.secret = cursor.fetchone()[0]

        if description != "":
            self.description = description
        else:
            query = 'SELECT description FROM accounts WHERE name = %s'
            values = (self.username,)
            cursor.execute(query, values)
            self.description = cursor.fetchone()[0]

    def saveToDb(self):
        query = 'INSERT INTO accounts (name, secret, description) VALUES (%s, %s, %s)'
        values = (self.username, self.secret, self.description)
        cursor.execute(query, values)
        conn.commit()

        query = 'INSERT INTO emails (name, address) VALUES (%s, %s)'
        values = (self.username, self.username)
        cursor.execute(query, values)
        conn.commit()
    
    def removeFromDb(self):
        query = "UPDATE accounts SET secret = NULL, active = 0 WHERE name = %s"
        values = (self.username,)
        cursor.execute(query, values)
        conn.commit()

        subprocess.run(["/opt/stalwart-mail/bin/stalwart-cli", "-c", os.getenv("GHOSTINBOX_ADMIN_CREDENTIALS"), "-u", "http://localhost:8083", "database", "delete", self.username])

    def verifyPassword(self, password):
        return bcrypt.checkpw(password.encode(), self.secret.encode())

    def setPassword(self, new_password):
        salt = bcrypt.gensalt()
        secret = bcrypt.hashpw(new_password.encode(), salt).decode()
        
        query = 'UPDATE accounts SET secret=%s WHERE name=%s'  
        values = (secret, self.username)
        cursor.execute(query, values)
        conn.commit()

        self.secret = secret

    def setDisplayName(self, description):
        query = 'UPDATE accounts SET description=%s WHERE name=%s'
        values = (description, self.username)
        cursor.execute(query, values)
        conn.commit()

        self.description = description

    def getDictAccount(self):
        return {"username": self.username, "displayName": self.description}

    @staticmethod
    def getUsernameFromToken(token):
        url = 'http://localhost:8083/.well-known/jmap'
        headers = {'Authorization': token}
        response = requests.get(url, headers=headers)
        
        if response.status_code == 200:
            return {"username": response.json()["username"]}
        else:
            return {"status_code": response.status_code}



class Ghost(Account):
    def __init__(self, username="", description="", secret="", expiration_time=0):
        if username != "":
            super().__init__(username, description, secret)
        else:
            super().__init__(Ghost.findFreeAddress(), description, secret)

        if expiration_time != 0:
            self.expiration_time = expiration_time
        else:
            query = 'SELECT expiration_time FROM accounts WHERE name = %s'
            values = (self.username,)
            cursor.execute(query, values)
            self.expiration_time = int(cursor.fetchone()[0])

    def saveToDb(self):
        query = 'INSERT INTO accounts (name, secret, description, expiration_time) VALUES (%s, %s, %s, %s)'
        values = (self.username, self.secret, self.description, self.expiration_time)
        cursor.execute(query, values)
        conn.commit()

        query = 'INSERT INTO emails (name, address) VALUES (%s, %s)'
        values = (self.username, self.username)
        cursor.execute(query, values)
        conn.commit()

    @staticmethod
    def generatePassword():
        password_length = secrets.randbelow(21) + 40  # Random length between 40 and 60
        characters = string.ascii_letters + string.digits + string.punctuation
        secure_password = ''.join(secrets.choice(characters) for _ in range(password_length))
        return secure_password

    @staticmethod
    def findFreeAddress():
        query = "SELECT MAX(name) FROM accounts WHERE name LIKE 'ghost%';"
        cursor.execute(query)
        return "ghost" + str(int(cursor.fetchone()[0].split("@")[0][5:])+1) + "@ghostinbox.pl"


@app.route('/account/create', methods=['POST'])
def createAccount():
    data = request.get_json()
      
    if not checkCaptcha(request.headers.get('Captchatoken')):
        return '', 403

    if 'username' not in data or 'password' not in data:
        return jsonify({'error': 'Missing argument(s)'}), 400

    salt = bcrypt.gensalt()
    secret = bcrypt.hashpw(data['password'].encode(), salt).decode()
 
    new_account = Account(username=data['username'], secret=secret, description=data['displayName'])
    new_account.saveToDb()

    return jsonify({'message': 'Account created successfully'}), 201


@app.route('/account/delete', methods=['DELETE'])
def deleteAccount():
    data = request.get_json()

    username_request = Account.getUsernameFromToken(request.headers.get("Authorization"))

    if "username" in username_request.keys():
        account = Account(username=username_request['username'])
    else:
        return jsonify({'message': 'Request failed'}), username_request["status_code"]
   
    if not account.verifyPassword(data["password"]):
        return jsonify({'message': 'Request failed'}), 403

    account.removeFromDb()

    return '', 204


@app.route('/account/get', methods=['GET'])
def getAccount():
    username_request = Account.getUsernameFromToken(request.headers.get("Authorization"))

    if "username" in username_request.keys():
        account = Account(username=username_request['username'])

    return jsonify(account.getDictAccount()), 200


@app.route('/account/password', methods=['POST'])
def changePassword():
    data = request.get_json()

    username_request = Account.getUsernameFromToken(request.headers.get("Authorization"))

    if "username" in username_request.keys():
        account = Account(username=username_request['username'])
    else:
        return jsonify({'message': 'Request failed'}), username_request["status_code"]

    if 'password' not in data or 'newPassword' not in data:
        return jsonify({'error': 'Missing argument(s)'}), 400

    if account.verifyPassword(data["password"]):
        account.setPassword(data["newPassword"])
    else:
        return jsonify({'message': 'Request failed'}), 403
        
    return jsonify(account.getDictAccount()), 204


@app.route('/account/display-name', methods=['POST'])
def changeDisplayName():
    username_request = Account.getUsernameFromToken(request.headers.get("Authorization"))
    account = Account(username=username_request['username'])

    data = request.get_json()
    account.setDisplayName(data["displayName"])

    return '', 204


######## GHOST #########
@app.route('/ghost/create', methods=['POST'])
def createGhost():
    data = request.get_json()

    if not checkCaptcha(request.headers.get('Captchatoken')):
        return '', 403

    if 'expirationTime' not in data or 'displayName' not in data:
        return jsonify({'error': 'Missing argument(s)'}), 400

    password = Ghost.generatePassword()

    salt = bcrypt.gensalt()
    secret = bcrypt.hashpw(password.encode(), salt).decode()

    new_account = Ghost(secret=secret, description=data['displayName'], expiration_time=data['expirationTime'])
    new_account.saveToDb()
    return jsonify({'Authorization': base64.b64encode((new_account.username+":"+password).encode()).decode()}), 201

def checkCaptcha(token):
    captcha_url = 'https://www.google.com/recaptcha/api/siteverify'
    secret_key = os.getenv("GHOSTINBOX_CAPTCHA_SECRET")
    payload = {
       'secret': secret_key,
       'response': token
    }
    response = requests.post(captcha_url, data=payload)
    result = response.json()
    return result.get('success', False) 


@app.route('/ghost/delete', methods=['DELETE'])
def deleteGhost():

    username_request = Account.getUsernameFromToken(request.headers.get("Authorization"))

    if "username" in username_request.keys():
        account = Ghost(username=username_request['username'])
    else:
        return jsonify({'message': 'Request failed'}), username_request["status_code"]

    account.removeFromDb()

    return '', 204

if __name__ == '__main__':
    app.run(debug=True, port=os.getenv("GHOSTINBOX_PORT"))
